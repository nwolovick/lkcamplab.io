---
title: "Home"
date: 2018-03-24T19:55:15-03:00
draft: false
---

Lkcamp is a Linux Kernel study group open for everyone to participate.

Please, see [About](/about) and [Documentation]
(https://lkcamp.gitlab.io/lkcamp_docs/) pages for more information and how to
get started.

# Please edit our page

We are all volunteers, any help is welcomed.
The code of this page is available at [https://gitlab.com/lkcamp](https://gitlab.com/lkcamp)
If you want to help us to improve this page, please send us Pull Requests.

# Practical information

## Remote participation
- **Live stream:** [YouTube IC channel]
(https://www.youtube.com/channel/UCraCE6iWUcFCJSp-vmO1D3A/videos) 
- **Mailing list:** [lkcamp@lists.libreplanetbr.org]
(https://lists.libreplanetbr.org/mailman/listinfo/lkcamp)
- **IRC Channel:** #lkcamp @ Freenode
- **Telegram (IRC bridge):** [https://t.me/lkcamp](https://t.me/lkcamp)

## Round 2

### Attending the meetings
- **When:** Every Tuesday at 19h (Sao Paulo time) starting on 16 Oct 2018
- **Where:** [room 352 IC3.5 @ Unicamp (Campinas/SP Brazil)](https://www.openstreetmap.org/way/95011172)

### Hacking meetings
- **When:** Every Wednesday at 19h (Sao Paulo time) starting on 17 Oct 2018
- **Where:** [room 352 IC3.5 @ Unicamp (Campinas/SP Brazil)](https://www.openstreetmap.org/way/95011172)
- **Required:** Bring your own computer with sudo powers

### Meetings (Phase 1)

- [M1: Group presentation](https://lkcamp.gitlab.io/lkcamp_docs/phase_1/M01/)
    - When: 16/Out/2018
    - Presenter: Guilherme Gallo / Helen Koike
    - [video](https://www.youtube.com/watch?v=QbFDGzv1gUU),
    - [slides](/slides/round2/lkcamp-M1.odp),
    - English subtitle: no

- [M2: Device Drivers](https://lkcamp.gitlab.io/lkcamp_docs/phase_1/M02/)
    - When: 23/Out/2018
    - Presenter: Lucas Magalhães
    - [video](https://www.youtube.com/watch?v=cxVJLdYmVZ4),
    - [slides](https://gitlab.com/lucmaga/talks/blob/58e504141106dbb20deaa9a37c2599de5220a837/M2/presentation.pdf),
    - English subtitle: no

- M2.5: Git Basics
    - When: 30/Out/2018
    - Presenter: Lucas Magalhães / Helen Koike
    - [video](https://www.youtube.com/watch?v=GOHm0haXfUo),
    - [slides](https://gitlab.com/lucmaga/talks/blob/58e504141106dbb20deaa9a37c2599de5220a837/git/presentation.pdf),
    - English subtitle: no

- [M3: The Linux Kernel Development Cycle](https://lkcamp.gitlab.io/lkcamp_docs/phase_1/M03/)
    - When: 06/Nov/2018
    - Presenter: Guilherme Gallo
    - [slides](/slides/round2/lkcamp-M3.odp),
    - English subtitle: no

## Round 1

### Attending the meetings
- **When:** Every Tuesday at 19h (Sao Paulo time) until 2 Oct 2018
- **Where:** room 352 IC3.5 @ Unicamp (Campinas/SP Brazil)
- **Required:** Bring your own computer with sudo powers

Check our videos from previous rounds at [the archived](/archived/) page
